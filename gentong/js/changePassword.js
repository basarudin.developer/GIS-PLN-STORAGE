$(document).ready(function() {
     $('#btnUpdateConfirmOK').click(function() {
          $.ajax({
               type: 'GET',
               url: "changePassword.svr.pln?action=update",
               dataType:"json",
               data: $("#frmUpdatePassword").serialize(),
               cache:false,
               //jika complete maka
               complete:
               function(data,status){
                    return false;
               },
               success:
                    function(msg,status){
                         //alert(msg);
                         if(msg.result == 'success'){
                              jSuccess(msg.desc +'<br /> Dengan menekan tombol OK anda akan logout.', 'Sukses',function(result){
                                   window.location="logout.pln";
                              });
                         }else{
                              $("#msgContainer").attr("class","ui-state-error  ui-corner-all");
                         }
                         $("#msgContainer").hide();
                         $("#msgContainer").html(msg.desc);
                         $("#msgContainer").fadeIn('slow', function() {
                              // Animation complete
                         });
                    },
               //untuk sementara tidak digunakan
               error:
                    function(msg,textStatus, errorThrown){
                         jError('Terjadi error saat proses kirim data', 'Konfirmasi');
                    }
          });//end of ajax
     });
}); 
