$(document).ready(function(){
     
     //tombol list hapus user
     $(".imgDelete").click(function(){
          userID = $(this).attr('id');
          realName = $(this).attr('alt');
          jConfirm('Benar Anda ingin menghapus user <b>'+ realName +'</b>?', 'Konfirmasi',
               function(result){
                    if(result == true){

                         $.ajax({
                              type: 'POST',
                              url: "user.svr.pln",
                              dataType:"json",
                              data: "action=deleteuser&userID="+userID,
                              cache:false,
                              //jika complete maka
                              complete:
                              function(data,status){
                                   return false;
                              },
                              success:
                                   function(msg,status){
                                        if(msg.result == 'success'){
                                             $("#msgContainer").attr("class","ui-state-highlight   ui-corner-all");
                                             jSuccess(msg.desc, 'Sukses',function(result){
                                                  window.location = "user.pln?action=list";
                                             });
                                        }else{
                                             $("#msgContainer").attr("class","ui-state-error  ui-corner-all");

                                        }
                                        $("#msgContainer").hide();
                                        $("#msgContainer").html(msg.desc);
                                        $("#msgContainer").fadeIn('slow', function() {
                                             // Animation complete
                                        });
                                   },
                              //untuk sementara tidak digunakan
                              error:
                                   function(msg,textStatus, errorThrown){
                                        jError('Terjadi error saat proses kirim data', 'Konfirmasi');
                                   }
                         });//end of ajax
                    }
          });
                    

     });
     //tombol buat user
     $("#btnCreateUser").click(function(){
          $.ajax({
               type: 'POST',
               url: "user.svr.pln",
               dataType:"json",
               data: $("#frmUserCreate").serialize(),
               cache:false,
               //jika complete maka
               complete:
               function(data,status){
                    return false;
               },
               success:
                    function(msg,status){
                        
                         if(msg.result == 'success'){
                              $("#msgContainer").attr("class","ui-state-highlight   ui-corner-all");
                              jSuccess(msg.desc, 'Sukses',function(result){
                                   window.location = "user.pln?action=list";
                              });
                         }else{
                              $("#msgContainer").attr("class","ui-state-error  ui-corner-all");
                         }
                         $("#msgContainer").hide();
                         $("#msgContainer").html(msg.desc);
                         $("#msgContainer").fadeIn('slow', function() {
                              // Animation complete
                         });
                    },
               //untuk sementara tidak digunakan
               error:
                    function(msg,textStatus, errorThrown){
                         jError('Terjadi error saat proses kirim data', 'Konfirmasi');
                    }
          });//end of ajax
     });
});
