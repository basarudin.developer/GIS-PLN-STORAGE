$(document).ready(function() {
     $('#btnUpdateConfirmOK').click(function() {
          //$('#rssContainerUpdate').hide();

          $.ajax({
               type: 'GET',
               url: "profil.svr.pln?action=update",
               dataType:"json",
               data: $("#frmUpdatePassword").serialize(),
               cache:false,
               //jika complete maka
               complete:
               function(data,status){
                    return false;
               },
               success:
                    function(msg,status){
                         if(msg.result == 'success'){
                              
                              $("#msgContainer").attr("class","alert alert-success");
                              
                              $("#msgContainer").html(msg.desc);
                              jSuccess(msg.desc +'<br /> Dengan menekan tombol OK anda akan logout.', 'Sukses',function(result){
                                   window.location="logout.pln";
                              });
                         }else{
                              
                              $("#msgContainer").attr("class","alert alert-danger");
                         }
                         $("#msgContainer").hide();
                         $("#msgContainer").html(msg.desc);
                         $("#msgContainer").fadeIn('slow', function() {
                              // Animation complete
                         });
                    },
               //untuk sementara tidak digunakan
               error:
                    function(msg,textStatus, errorThrown){
                         jError('Terjadi error saat proses kirim data', 'Konfirmasi');
                    }
          });//end of ajax
     });
     
     $('#btnUpdateHP').click(function() {
          $.ajax({
               type: 'GET',
               url: "profil.svr.pln?action=updatePhone",
               dataType:"json",
               data: $("#frmUpdatePhone").serialize(),
               cache:false,
               //jika complete maka
               complete:
               function(data,status){
                    return false;
               },
               success:
                    function(msg,status){
                         if(msg.result == 'success'){
                              
                              $("#msgContainer").attr("class","alert alert-success");
                              
                              $("#msgContainer").html(msg.desc);
                              jSuccess(msg.desc +'<br /> No HP sudah berhasi diganti.', 'Sukses',function(result){
                                   window.location="profil.pln";
                              });
                         }else{
                              
                              $("#msgContainer").attr("class","alert alert-danger");
                         }
                         $("#msgContainer").hide();
                         $("#msgContainer").html(msg.desc);
                         $("#msgContainer").fadeIn('slow', function() {
                              // Animation complete
                         });
                    },
               //untuk sementara tidak digunakan
               error:
                    function(msg,textStatus, errorThrown){
                         jError('Terjadi error saat proses kirim data', 'Konfirmasi');
                    }
          });//end of ajax
     });
     
}); 