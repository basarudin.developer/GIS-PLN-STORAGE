
$(document).ready(function() {
     //untuk input rupiah
     
        $(".date").datepicker({
                dateFormat: 'dd/mm/yy',
                constrainInput: false
            }).css({
                "position": "relative",
                "z-index": 20
            });
     $("#txtRupiah").maskMoney({
          prefix:'', 
          allowNegative: false, 
          thousands:'.', 
          decimal:',', 
          affixesStay: false,
          precision:0
     });
     $("#btnBack").click(function(){
          window.location = "inventory.satuan.pln?action=list";
     });
}); 