
$(document).ready(function() {
     
     $(".imgDelete").click(function(){
          
          $("#vUploadID").val($(this).attr('alt'));
          $("#vAction").val('delete');
          jConfirm('Apakah anda ingin menghapus upload ini?', 'Konfirmasi',function(result){
               if(result == true){
                    //remove all the class add the messagebox classes and start fading
                    $.blockUI({ message: ' Menunggu...' });
                    $.ajax({
                         type: 'POST',
                         url: "transaksi.svr.pln",
                         dataType:"json",
                         data: $("#formDirect").serialize(),
                         cache:false,
                         //jika complete maka
                         complete:
                         function(data,status){
                              $.unblockUI();
                              return false;
                         },
                         success:
                              function(msg,status){
                                   if(msg.result == 'success'){
                                        jSuccess(msg.desc, 'Sukses',function(result){
                                             window.location = "transaksi.pln?action=upload";
                                        });

                                   }else{
                                        jError(msg.desc, 'Error');
                                   }
                              },
                         //untuk sementara tidak digunakan
                         error:
                              function(msg,textStatus, errorThrown){
                                   jError('Terjadi error saat proses kirim data', 'Konfirmasi');
                              }
                    });//end of ajax
               }else{
                    
               }
          });
     });
     //tombol buat news
     $("#btnUpload").click(function(){
          $.ajax({
               type: 'POST',
               url: "transaksi.svr.pln",
               dataType:"json",
               data: $("#frmUploadBerkas").serialize(),
               cache:false,
               //jika complete maka
               complete:
               function(data,status){
                    return false;
               },
               success:
                    function(msg,status){
                         //alert(msg);
                         if(msg.result == 'success'){
                              $("#msgContainer").attr("class","alert alert-success");
                                   window.location = "transaksi.pln?action=upload";
                         }else{
                              $("#msgContainer").attr("class","alert alert-danger");
                         }
                         $("#msgContainer").hide();
                         $("#msgContainer").html(msg.desc);
                         $("#msgContainer").fadeIn('slow', function() {
                              // Animation complete
                         });
                    },
               //untuk sementara tidak digunakan
               error:
                    function(msg,textStatus, errorThrown){
                         jError('Terjadi error saat proses kirim data', 'Konfirmasi');
                    }
          });//end of ajax
     });
     
     $("#btnFinish").click(function(){
          jConfirm('Apakah anda tidak ingin mengupload beberapa dokumen lagi?', 'Konfirmasi',function(result){
               if(result == true){
                     window.location = "transaksi.pln?action=list";
               }
          });
     });
     
}); 