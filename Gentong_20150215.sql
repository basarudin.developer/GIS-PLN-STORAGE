-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.41


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema gentong
--

CREATE DATABASE IF NOT EXISTS gentong;
USE gentong;

--
-- Temporary table structure for view `vUserAccessLevel`
--
DROP TABLE IF EXISTS `vUserAccessLevel`;
DROP VIEW IF EXISTS `vUserAccessLevel`;
CREATE TABLE `vUserAccessLevel` (
  `userID` varchar(20),
  `realname` varchar(100),
  `groupID` varchar(20),
  `name` varchar(25),
  `pageID` varchar(6),
  `filename` varchar(255),
  `active` tinyint(1)
);

--
-- Temporary table structure for view `vUserMenu`
--
DROP TABLE IF EXISTS `vUserMenu`;
DROP VIEW IF EXISTS `vUserMenu`;
CREATE TABLE `vUserMenu` (
  `userID` varchar(20),
  `realname` varchar(100),
  `groupID` varchar(20),
  `name` varchar(25),
  `status` tinyint(1),
  `id` smallint(3),
  `parentID` smallint(3),
  `icon` varchar(256),
  `title` varchar(100),
  `url` varchar(100),
  `menuOrder` tinyint(3) unsigned,
  `keterangan` varchar(20)
);

--
-- Definition of table `appUnit`
--

DROP TABLE IF EXISTS `appUnit`;
CREATE TABLE `appUnit` (
  `idUnit` varchar(5) NOT NULL,
  `unitName` varchar(20) DEFAULT NULL,
  `parent` varchar(5) DEFAULT NULL,
  `unitAddress` varchar(32) DEFAULT NULL,
  `unitTelp` varchar(12) DEFAULT NULL,
  `unitNpwp` varchar(20) DEFAULT NULL,
  `unitStatus` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`idUnit`),
  KEY `fk_appUnit_appUnit_parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='master unit yang ada di tiap 2 area';

--
-- Dumping data for table `appUnit`
--

/*!40000 ALTER TABLE `appUnit` DISABLE KEYS */;
INSERT INTO `appUnit` (`idUnit`,`unitName`,`parent`,`unitAddress`,`unitTelp`,`unitNpwp`,`unitStatus`) VALUES 
 ('51100','APJ SURABAYA UTARA',NULL,'Jl. Gemblongan No.64','031-5340151','01-001-629-3-631-008',1),
 ('51101','UPJ INDRAPURA','51100','Jl. Indrapura No.48','031-3538691','01-001-629-3-631-008',1),
 ('51102','UPJ PLOSO','51100','Jl. Ploso Timur III/01','031-3813911','01-001-629-3-631-008',1),
 ('51103','UPJ TANDES','51100','Jl. Margomulyo IndahRaya Kav E34','031-7491581','01-001-629-3-631-008',1),
 ('51104','UPJ PERAK','51100','Jl. Tanjung Sadari No. 82','031-3559400','01-001-629-3-631-008',1),
 ('51105','UPJ KENJERAN','51100','Jl. Wiratno No. 53','031-3817285','01-001-629-3-631-008',1),
 ('51106','UPJ EMBONG WUNGU','51100','Jl. Embong Wungu No 4 ','031-5320507','01-001-629-3-631-008',1);
/*!40000 ALTER TABLE `appUnit` ENABLE KEYS */;


--
-- Definition of table `berkas`
--

DROP TABLE IF EXISTS `berkas`;
CREATE TABLE `berkas` (
  `berkasID` int(11) NOT NULL,
  `berkasTransaksi` int(7) DEFAULT NULL,
  `berkasFileName` varchar(45) DEFAULT NULL,
  `berkasPath` varchar(45) DEFAULT NULL,
  `berkasUploadDate` date DEFAULT NULL,
  `berkasUploadUser` varchar(20) DEFAULT NULL,
  `berkasJenis` varchar(20) DEFAULT NULL,
  `berkasKeterangan` varchar(45) DEFAULT NULL,
  `berkasStatus` tinyint(1) DEFAULT '1' COMMENT '0:disable',
  PRIMARY KEY (`berkasID`),
  KEY `fk_berkas_berkasTrans_transaksi_trans_idx` (`berkasTransaksi`),
  CONSTRAINT `fk_berkas_berkasTrans_transaksi_trans` FOREIGN KEY (`berkasTransaksi`) REFERENCES `transaksi` (`transID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berkas`
--

/*!40000 ALTER TABLE `berkas` DISABLE KEYS */;
/*!40000 ALTER TABLE `berkas` ENABLE KEYS */;


--
-- Definition of table `counterDB`
--

DROP TABLE IF EXISTS `counterDB`;
CREATE TABLE `counterDB` (
  `tableName` varchar(64) NOT NULL,
  `initial` varchar(5) NOT NULL,
  `lastUpdate` date DEFAULT NULL,
  `nextID` int(4) DEFAULT '1',
  `resetID` int(1) DEFAULT NULL,
  `notes` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`tableName`),
  UNIQUE KEY `initial` (`initial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='0 tanpa reset.1 perhari ,2 perminggu 3 perbulan 4 pertahun';

--
-- Dumping data for table `counterDB`
--

/*!40000 ALTER TABLE `counterDB` DISABLE KEYS */;
INSERT INTO `counterDB` (`tableName`,`initial`,`lastUpdate`,`nextID`,`resetID`,`notes`) VALUES 
 ('log','LOG','2015-02-10',2,1,NULL),
 ('user','USER','2012-10-15',2,1,NULL);
/*!40000 ALTER TABLE `counterDB` ENABLE KEYS */;


--
-- Definition of table `departemen`
--

DROP TABLE IF EXISTS `departemen`;
CREATE TABLE `departemen` (
  `deptID` int(8) NOT NULL AUTO_INCREMENT,
  `deptName` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `deptParent` int(8) DEFAULT NULL,
  `deptStatus` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`deptID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departemen`
--

/*!40000 ALTER TABLE `departemen` DISABLE KEYS */;
INSERT INTO `departemen` (`deptID`,`deptName`,`deptParent`,`deptStatus`) VALUES 
 (1,'Koperasi',NULL,NULL);
/*!40000 ALTER TABLE `departemen` ENABLE KEYS */;


--
-- Definition of table `documentArsip`
--

DROP TABLE IF EXISTS `documentArsip`;
CREATE TABLE `documentArsip` (
  `IDAID` int(11) NOT NULL AUTO_INCREMENT,
  `transaksiID` varchar(30) NOT NULL,
  `jenisDoc` varchar(30) NOT NULL,
  `IDADocumentPath` varchar(256) NOT NULL,
  `IDAUserUpload` varchar(20) NOT NULL,
  `IDADateUpload` datetime NOT NULL,
  `IDAKeterangan` tinytext NOT NULL,
  `IDAStatus` tinyint(4) NOT NULL,
  PRIMARY KEY (`IDAID`),
  KEY `transaksiID` (`transaksiID`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `documentArsip`
--

/*!40000 ALTER TABLE `documentArsip` DISABLE KEYS */;
INSERT INTO `documentArsip` (`IDAID`,`transaksiID`,`jenisDoc`,`IDADocumentPath`,`IDAUserUpload`,`IDADateUpload`,`IDAKeterangan`,`IDAStatus`) VALUES 
 (1,'1','SPK','file/pdf/1_d2c79203a7344e6aa767e39945f36282.pdf','adminkoperasi','2014-06-11 21:31:42','no 032344',1),
 (2,'1','Kuitansi','file/pdf/1_eaa0dc34490afcb96aa573637064d618.pdf','adminkoperasi','2014-06-11 21:31:53','kuintasi no 34',1),
 (3,'2','Kuitansi','file/pdf/2_c7de53bded0aaf8a6ecc0ecf29aaf857.pdf','adminkoperasi','2014-06-11 21:42:24','90323',0),
 (4,'2','Lampiran','file/pdf/2_1306f4873ff4da36d3eaa72709d96449.pdf','adminkoperasi','2014-06-12 09:31:23','34',1),
 (5,'4','Kuitansi','file/pdf/4_d488d5a6b2e005e1e831afbfdf8fd637.pdf','adminkoperasi','2014-06-12 09:32:56','3',1),
 (6,'1','Kuitansi','file/pdf/1_b4ba22f7b7615b95b7a0487baba7a4a3.pdf','adminkoperasi','2014-06-12 13:42:24','34',1),
 (7,'2','Kuitansi','file/pdf/2_a9e1eadf789fc32720820710109df29a.pdf','adminkoperasi','2014-06-12 13:43:27','45',1),
 (8,'3','Kuitansi','file/pdf/3_b94498abe3db0d0fe80080ba68f94a00.pdf','adminkoperasi','2014-06-12 13:50:36','34',1),
 (9,'4','Kuitansi','file/pdf/4_94e6e37eceab5857c4f904e3a52c3952.pdf','adminkoperasi','2014-06-12 14:57:56','3',1),
 (10,'4','Kuitansi','file/pdf/4_eb99613e926f0665adc83a03d56032a5.pdf','adminkoperasi','2014-06-12 14:58:27','23',1),
 (11,'4','Kuitansi','file/pdf/4_247a25e65ba36fdb06b49e87de343e7b.pdf','adminkoperasi','2014-06-12 15:00:25','4',1),
 (12,'4','Kuitansi','file/pdf/4_2f4eed27322f83cf59b7106f47e3ed0a.pdf','adminkoperasi','2014-06-12 15:01:03','4',1),
 (13,'5','Kuitansi','file/pdf/5_a6bce79bdf98785652033aa81d41675a.pdf','adminkoperasi','2014-06-12 15:02:46','5',1),
 (14,'5','Kuitansi','file/pdf/5_23f41165c64553246d0d01b2d31a00bb.pdf','adminkoperasi','2014-06-12 15:04:23','45',1),
 (15,'5','Kuitansi','file/pdf/5_fdf9843288a00d945ed6d0859972d286.pdf','adminkoperasi','2014-06-12 15:09:46','45',1),
 (16,'5','Kuitansi','file/pdf/5_0eb996a7a0097a037765b37d36a18168.pdf','adminkoperasi','2014-06-12 15:09:50','34',1),
 (17,'5','Kuitansi','file/pdf/5_295787b0f8a9313e17f04f5520981910.pdf','adminkoperasi','2014-06-12 15:10:58','4',1),
 (18,'6','SPK','file/pdf/6_6955e6071fc22836bd5da60a2f909d14.pdf','adminkoperasi','2014-07-02 12:03:05','34',1),
 (19,'7','Kuitansi','file/pdf/7_2ac4cce05d8085f05a6bf727ed7240fc.pdf','adminkoperasi','2014-07-02 12:03:28','3434',1),
 (20,'7','Kuitansi','file/pdf/7_db522cf7d1daaa48aa475ed7443bad55.pdf','adminkoperasi','2014-07-02 12:03:42','4545',1),
 (21,'8','Kuitansi','file/pdf/8_7400c638ca5a105ea346572a052f57ea.pdf','adminkoperasi','2015-01-19 10:39:33','kwitans',1),
 (22,'8','SPK','file/pdf/8_29562506c1298f233392d48229883a48.pdf','adminkoperasi','2015-01-19 10:39:49','sd',1),
 (23,'9','Lain - lain','file/pdf/9_f047e25019ca82a5e3ca3c41490654fc.pdf','adminkoperasi','2015-01-19 10:45:17','sebelum',1),
 (24,'9','Lampiran','file/pdf/9_7faf50b0f9730ba26410819462bb0e6f.pdf','adminkoperasi','2015-01-19 10:45:42','sesudah',1),
 (25,'9','Kuitansi','file/pdf/9_025f875d135f03d4f3f2603292c8f94a.pdf','adminkoperasi','2015-01-19 10:48:02','uang muka',1),
 (26,'10','Foto Sebelum Pekerjaan','file/pdf/10_f94c9b97d075b8d3f55106425a039d61.pdf','adminkoperasi','2015-01-27 13:31:31','foto sebelum',1),
 (27,'10','Nota Dinas / Memo','file/pdf/10_8999c3f898d0e7271215cb3069ed48d4.pdf','adminkoperasi','2015-01-27 13:32:22','dd',1),
 (28,'11','Foto Sebelum Pekerjaan','file/pdf/11_f3687c584c657dbf9423453bedbd7f9e.pdf','adminkoperasi','2015-01-27 18:41:53','sebelum diganti',1),
 (29,'11','Foto Sesudah  Pekerjaan','file/pdf/11_88263edefb3807082f6d7e89b811ace0.pdf','adminkoperasi','2015-01-27 18:42:17','selesai beroperasi',1);
/*!40000 ALTER TABLE `documentArsip` ENABLE KEYS */;


--
-- Definition of table `group`
--

DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `groupID` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(25) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `notes` text NOT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='group id untuk level akses.';

--
-- Dumping data for table `group`
--

/*!40000 ALTER TABLE `group` DISABLE KEYS */;
INSERT INTO `group` (`groupID`,`name`,`active`,`notes`) VALUES 
 ('GRP00000000000000000','common',1,'common user'),
 ('GRP_KOPERASI_ADMIN','GRP_KOPERASI_ADMIN',1,''),
 ('GRP_KOPERASI_ENTRY','Group entry transaksi',1,'');
/*!40000 ALTER TABLE `group` ENABLE KEYS */;


--
-- Definition of table `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `logID` varchar(20) NOT NULL,
  `logUserID` varchar(20) DEFAULT NULL,
  `logUserAgent` varchar(200) DEFAULT NULL,
  `logTime` datetime DEFAULT NULL,
  `logClientIP` varchar(15) DEFAULT NULL,
  `logClientURL` varchar(128) DEFAULT NULL,
  `logModulID` varchar(8) NOT NULL,
  `logDetail` varchar(200) NOT NULL,
  PRIMARY KEY (`logID`),
  KEY `fk_log_user_logUserID` (`logUserID`),
  KEY `fk_log_logUserID_user_userID_idx` (`logUserID`),
  CONSTRAINT `fk_log_logUserID_user_userID` FOREIGN KEY (`logUserID`) REFERENCES `user` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='table keterangan histori / log';

--
-- Dumping data for table `log`
--

/*!40000 ALTER TABLE `log` DISABLE KEYS */;
INSERT INTO `log` (`logID`,`logUserID`,`logUserAgent`,`logTime`,`logClientIP`,`logClientURL`,`logModulID`,`logDetail`) VALUES 
 ('LOG2014061200004','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-06-12 14:02:17','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2014061200005','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-06-12 14:10:10','::1','/gentong/transaksi.pln?action=list','TRANSAKS','Membuat transaksi baru.'),
 ('LOG2014061200006','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-06-12 14:10:14','::1','/gentong/transaksi.pln?action=list','TRANSAKS','Membuat transaksi baru.'),
 ('LOG2014061200007','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-06-12 14:11:21','::1','/gentong/transaksi.pln?action=list','TRANSAKS','Mengupload file berkas Kuitansi.'),
 ('LOG2014061200008','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-06-12 14:25:07','::1','/gentong/profil.svr.pln?action=updatePhone&_=1402557907302&txtPhone=0822256576663','PROFIL','Mengganti no telp 0822256576663.'),
 ('LOG2014061200009','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-06-12 14:25:14','::1','/gentong/profil.svr.pln?action=updatePhone&_=1402557914086&txtPhone=082225657666','PROFIL','Mengganti no telp 082225657666.'),
 ('LOG2014070100001','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36','2014-07-01 18:13:08','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2014070100002','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.153 Safari/537.36','2014-07-01 18:14:27','::1','/gentong/transaksi.svr.pln','TRANSAKS','Membuat transaksi baru.'),
 ('LOG2014070200001','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-07-02 11:02:32','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2014070200002','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-07-02 11:02:41','::1','/gentong/profil.svr.pln?action=update&_=1404273761698&txtOldPassword=123456&txtNewPassword=adminkoperasi&txtNewPasswordConfirm=a','PROFIL','Mengganti password baru.'),
 ('LOG2014070200003','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-07-02 11:02:48','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2014070200004','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-07-02 11:03:29','::1','/gentong/transaksi.pln?action=list','TRANSAKS','Mengupload file berkas SPK.'),
 ('LOG2014070200005','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-07-02 11:03:47','::1','/gentong/transaksi.svr.pln','TRANSAKS','Membuat transaksi baru.'),
 ('LOG2014070200006','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-07-02 11:03:52','::1','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas Kuitansi.'),
 ('LOG2014070200007','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-07-02 11:04:06','::1','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas Kuitansi.'),
 ('LOG2014071100001','adminkoperasi','Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10','2014-07-11 18:56:34','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015011900001','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36','2015-01-19 10:38:08','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015011900002','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36','2015-01-19 10:39:09','::1','/gentong/transaksi.svr.pln','TRANSAKS','Membuat transaksi baru.'),
 ('LOG2015011900003','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36','2015-01-19 10:39:57','::1','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas Kuitansi.'),
 ('LOG2015011900004','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36','2015-01-19 10:40:13','::1','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas SPK.'),
 ('LOG2015011900005','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0','2015-01-19 10:43:18','10.5.11.147','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015011900006','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0','2015-01-19 10:44:50','10.5.11.147','/gentong/transaksi.svr.pln','TRANSAKS','Membuat transaksi baru.'),
 ('LOG2015011900007','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0','2015-01-19 10:45:41','10.5.11.147','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas Lain - lain.'),
 ('LOG2015011900008','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0','2015-01-19 10:46:06','10.5.11.147','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas Lampiran.'),
 ('LOG2015011900009','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0','2015-01-19 10:48:26','10.5.11.147','/gentong/transaksi.pln?action=list','TRANSAKS','Mengupload file berkas Kuitansi.'),
 ('LOG2015012700001','adminkoperasi','Mozilla/5.0 (Linux; Android 4.3; HM 1S Build/JLS36C) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.89 Mobile Safari/537.36','2015-01-27 10:56:41','10.5.11.130','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015012700002','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0','2015-01-27 12:12:48','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015012700003','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0','2015-01-27 13:31:16','::1','/gentong/transaksi.svr.pln','TRANSAKS','Membuat transaksi baru.'),
 ('LOG2015012700004','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0','2015-01-27 13:31:54','::1','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas Foto Sebelum Pekerjaan.'),
 ('LOG2015012700005','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0','2015-01-27 13:32:45','::1','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas Nota Dinas / Memo.'),
 ('LOG2015012700006','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36','2015-01-27 17:52:31','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015012700007','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0','2015-01-27 18:37:40','10.5.11.147','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015012700008','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0','2015-01-27 18:40:19','10.5.11.147','/gentong/transaksi.svr.pln','TRANSAKS','Membuat transaksi baru.'),
 ('LOG2015012700009','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0','2015-01-27 18:42:17','10.5.11.147','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas Foto Sebelum Pekerjaan.'),
 ('LOG2015012700010','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0','2015-01-27 18:42:41','10.5.11.147','/gentong/transaksi.pln?action=upload','TRANSAKS','Mengupload file berkas Foto Sesudah  Pekerjaan.'),
 ('LOG2015012700011','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.93 Safari/537.36','2015-01-27 19:52:50','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015020400001','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36','2015-02-04 11:58:07','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015020400002','adminkoperasi','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.16 (KHTML, like Gecko) Chrome/20.0.1207.0 PlayFreeBrowser/2.3.0.1 Safari/537.16','2015-02-04 12:00:01','10.5.11.130','/gentong/login.svr.pln','LOGIN','telah berhasil login'),
 ('LOG2015020400003','adminkoperasi','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.16 (KHTML, like Gecko) Chrome/20.0.1207.0 PlayFreeBrowser/2.3.0.1 Safari/537.16','2015-02-04 12:03:21','10.5.11.130','/gentong/transaksi.svr.pln','TRANSAKS','Membuat transaksi baru.'),
 ('LOG2015021000001','adminkoperasi','Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.111 Safari/537.36','2015-02-10 11:06:15','::1','/gentong/login.svr.pln','LOGIN','telah berhasil login');
/*!40000 ALTER TABLE `log` ENABLE KEYS */;


--
-- Definition of table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` smallint(3) NOT NULL AUTO_INCREMENT,
  `parentID` smallint(3) DEFAULT NULL COMMENT 'sebangai induk dari menu',
  `icon` varchar(256) NOT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL DEFAULT '',
  `menuOrder` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'memilah2 mana yang diletakan diatas / bawah',
  `keterangan` varchar(20) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_menu_menu_parentID` (`parentID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='menu links untuk URL.group id 0 boleh diakses siapa saja';

--
-- Dumping data for table `menu`
--

/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` (`id`,`parentID`,`icon`,`title`,`url`,`menuOrder`,`keterangan`,`status`) VALUES 
 (1,NULL,'images/menu/transaksi.png','Transaksi','transaksi.pln?action=list',0,NULL,1),
 (2,NULL,'images/menu/upload.png','Upload','transaksi.pln?action=create',1,NULL,1),
 (4,NULL,'images/menu/dashboard.png','Dashboard','dashboard.pln',3,NULL,1),
 (5,NULL,'images/menu/setting.png','Setting','',4,NULL,1);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


--
-- Definition of table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE `notification` (
  `notifID` int(5) NOT NULL AUTO_INCREMENT,
  `notifType` varchar(1) NOT NULL,
  `notifMessage` text NOT NULL,
  `notifLink` varchar(160) NOT NULL,
  `notifUseraTo` varchar(20) NOT NULL,
  `notifUserFrom` varchar(20) NOT NULL,
  `notifDateCreate` datetime NOT NULL,
  `notifLogo` varchar(100) NOT NULL,
  `notifStatus` varchar(1) NOT NULL DEFAULT '0' COMMENT '0:belum dibaca.1:sudah dibaca.2 dihapus',
  PRIMARY KEY (`notifID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;


--
-- Definition of table `pageRegistered`
--

DROP TABLE IF EXISTS `pageRegistered`;
CREATE TABLE `pageRegistered` (
  `pageID` varchar(6) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `notes` varchar(255) NOT NULL,
  PRIMARY KEY (`pageID`),
  KEY `idx_pageID_pageID` (`pageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='halaman yang hanya bisa diakses oleh user yang berhak';

--
-- Dumping data for table `pageRegistered`
--

/*!40000 ALTER TABLE `pageRegistered` DISABLE KEYS */;
INSERT INTO `pageRegistered` (`pageID`,`filename`,`notes`) VALUES 
 ('CHP001','changePassword.svr.pln',''),
 ('CHP002','changePassword.pln',''),
 ('DSB001','dashboard.pln',''),
 ('DSB002','dashboard.jumlah.pln',''),
 ('DSB003','dashboard.total.pln',''),
 ('IND001','index.pln','halaman utama'),
 ('MNU01','menu.pln',''),
 ('MNU02','sub.menu.pln',''),
 ('PDFUP1','upload file pdf transaksi',''),
 ('TRS001','transaksi.pln',''),
 ('TRS002','transaksi.svr.pln',''),
 ('TRS003','transaksi.create.pln',''),
 ('TRS004','transaksi.create.pln',''),
 ('TRS005','transaksi.upload.pln',''),
 ('TRS006','transaksi.detail.pln',''),
 ('USR002','user.pln',''),
 ('USR003','user.list.pln',''),
 ('USR004','user.create.pln',''),
 ('USR005','user.update.pln',''),
 ('USR006','user.delete.pln',''),
 ('USR007','user.info.pln','');
/*!40000 ALTER TABLE `pageRegistered` ENABLE KEYS */;


--
-- Definition of table `transaksi`
--

DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi` (
  `transID` int(7) NOT NULL AUTO_INCREMENT,
  `transNoSPK` varchar(45) DEFAULT NULL,
  `transTglMasuk` date DEFAULT NULL,
  `transUserRequest` varchar(45) DEFAULT NULL,
  `transVendor` varchar(45) DEFAULT NULL COMMENT 'vendor yang mengerjakan proyek',
  `transKeperluan` text,
  `transketerangan` text,
  `transNoOutner` varchar(12) DEFAULT NULL,
  `transRupiah` int(12) DEFAULT NULL,
  `transStatus` varchar(1) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '0' COMMENT ',0:baru masuk 1:proses;2:selesai',
  `transRayon` varchar(5) NOT NULL,
  `transJenisObject` varchar(31) NOT NULL,
  `transObjectSIGOID` varchar(31) NOT NULL,
  `transEksObject` varchar(100) NOT NULL,
  `transObjectLokasi` varchar(127) NOT NULL,
  `transObjectMerk` varchar(63) NOT NULL,
  `transPenyulang` varchar(63) NOT NULL,
  `transSection` varchar(63) NOT NULL,
  `transKontruksi` varchar(63) NOT NULL,
  `transKondisi` varchar(63) NOT NULL,
  `transGaransiBerakhir` date NOT NULL,
  `transJenisPekerjaan` varchar(63) NOT NULL,
  PRIMARY KEY (`transID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transaksi`
--

/*!40000 ALTER TABLE `transaksi` DISABLE KEYS */;
INSERT INTO `transaksi` (`transID`,`transNoSPK`,`transTglMasuk`,`transUserRequest`,`transVendor`,`transKeperluan`,`transketerangan`,`transNoOutner`,`transRupiah`,`transStatus`,`transRayon`,`transJenisObject`,`transObjectSIGOID`,`transEksObject`,`transObjectLokasi`,`transObjectMerk`,`transPenyulang`,`transSection`,`transKontruksi`,`transKondisi`,`transGaransiBerakhir`,`transJenisPekerjaan`) VALUES 
 (1,'SPK201407643','2014-06-11','Basar','PT Haleyora ','Pembangunan GI ',NULL,'H09',1000000,'0','','','','','','','','','','','0000-00-00',''),
 (2,'SPK0244313','2014-06-09','BASAR','PT PAMUNGKAS ','pemasangan genteng ',NULL,'T98',254000000,'0','','','','','','','','','','','0000-00-00',''),
 (3,'SPK099','2014-04-09','BASR','PT AJIPAMUNGKAS ','Perawatan komputer ',NULL,'HJ085656',54211811,'0','','','','','','','','','','','0000-00-00',''),
 (4,'SBU343/4556','2014-05-02','DWI','PT ASTINA ','Pembongkarang rumah dinas ',NULL,'H5',56000000,'0','','','','','','','','','','','0000-00-00',''),
 (5,'SBU34/523','2014-05-08','BUDI','PT LUHURABADI ','Pembuatan Website ',NULL,'Y56',8542000,'0','','','','','','','','','','','0000-00-00',''),
 (6,'Spk1234','2014-07-02','AHmad','pt ion ','pembangunan jembatan ',NULL,'ab123',1200000000,'0','','','','','','','','','','','0000-00-00',''),
 (7,'34','2014-07-16','343','3434 ','3434 ',NULL,'343',343,'0','','','','','','','','','','','0000-00-00',''),
 (8,'spk123','2015-01-20','Basar','PT mardika ','pembangunan rumah ',NULL,'A30 ',52000,'0','','','','','','','','','','','0000-00-00',''),
 (9,'0034','2014-12-10','fajar','gintung ','Pemeliharaan LBS tersebar ',NULL,'B23',315000000,'0','','','','','','','','','','','0000-00-00',''),
 (10,'s1','2015-01-01','Pegawai','Vendor','Perihal / Keterangan*',NULL,'Outner',0,'0','Rayon','Objek','ID Objek (SIGO)','Eks Objek','lamat / Lokasi Objek*','Merk','Penyulang','Section','Konstruksi','Kondisi','0000-00-00','Pekerjaan'),
 (11,'0010','2015-01-02','pras','CV. sumber','Pemeliharaan LBS',NULL,'1A11',0,'0','51106','LBS','','LBS','Wonorejo 3','MERLIN GERIN','SAWAHAN','2','TM 10','Baru','2015-09-15','Pemeliharaan'),
 (12,'spk0912','2015-02-05','basar','pt haleyora','perbaikan trafo',NULL,'a23',1,'0','indra','trafo','45335','2655','jln indrapuraa no 87','axs','indrapura','2','besi','baru','2015-03-05','pasangbaru3');
/*!40000 ALTER TABLE `transaksi` ENABLE KEYS */;


--
-- Definition of table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `userID` varchar(20) NOT NULL,
  `unitID` varchar(5) NOT NULL,
  `deptID` int(8) DEFAULT NULL,
  `reguID` int(8) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `realName` varchar(100) NOT NULL,
  `loginIP` varchar(36) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `mail` varchar(128) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `messenger` varchar(128) DEFAULT NULL,
  `join` date DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `lastSession` varchar(32) DEFAULT NULL,
  `language` varchar(2) NOT NULL,
  `gender` enum('m','f') NOT NULL DEFAULT 'm',
  `avatar` varchar(200) DEFAULT NULL,
  `notes` text,
  `isDC` tinyint(1) DEFAULT '1' COMMENT 'apakah join domain.0 tidak;1 join domain',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userID`),
  KEY `fk_user_appUnit1` (`unitID`),
  KEY `fk_user_unitID_unit_id_idx` (`unitID`),
  KEY `fk_user_deptID_departemen_deptID_idx` (`deptID`),
  CONSTRAINT `fk_user_deptID_departemen_deptID` FOREIGN KEY (`deptID`) REFERENCES `departemen` (`deptID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_unitID_unit_id` FOREIGN KEY (`unitID`) REFERENCES `appunit` (`idUnit`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='tabel user yang ada didalam system';

--
-- Dumping data for table `user`
--

/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`userID`,`unitID`,`deptID`,`reguID`,`username`,`realName`,`loginIP`,`password`,`mail`,`phone`,`messenger`,`join`,`lastLogin`,`lastSession`,`language`,`gender`,`avatar`,`notes`,`isDC`,`active`) VALUES 
 ('adminkoperasi','51100',1,NULL,'adminkoperasi','admin inventory',NULL,'ed3b285b5f234d9993fa10b1304ebe9f',NULL,'082225657666',NULL,NULL,'2015-02-10 11:06:15','tahm27b5h96eop119heutiktk5','','m',NULL,NULL,1,1),
 ('u51101','51101',NULL,NULL,'51101','APJ INDRAPURA',NULL,'f0342a4f9b0d5ab5ecc0870e115cd9e3',NULL,NULL,NULL,NULL,NULL,NULL,'','m',NULL,NULL,1,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;


--
-- Definition of table `userAccessLevel`
--

DROP TABLE IF EXISTS `userAccessLevel`;
CREATE TABLE `userAccessLevel` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `groupID` varchar(20) NOT NULL,
  `pageID` varchar(6) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `notes` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_groupID_pageID` (`groupID`,`pageID`),
  KEY `idx_groupID_pageID` (`groupID`,`pageID`),
  KEY `fk_userAccessLevel_group_groupID` (`groupID`),
  KEY `fk_userAccessLevel_pageRegisterd_pageID` (`pageID`),
  KEY `fk_userAccessLevel_pageID_pageRegistered_pageID_idx` (`pageID`),
  KEY `fk_userAccessLevel_pageID_pageID_group_groupID_idx` (`groupID`),
  CONSTRAINT `fk_userAccessLevel_pageID_pageID_group_groupID` FOREIGN KEY (`groupID`) REFERENCES `group` (`groupID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_userAccessLevel_pageID_pageRegistered_pageID` FOREIGN KEY (`pageID`) REFERENCES `pageregistered` (`pageID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='halaman yang hanya bisa diakses oleh user yang berhak';

--
-- Dumping data for table `userAccessLevel`
--

/*!40000 ALTER TABLE `userAccessLevel` DISABLE KEYS */;
INSERT INTO `userAccessLevel` (`id`,`groupID`,`pageID`,`active`,`notes`) VALUES 
 (1,'GRP00000000000000000','IND001',1,''),
 (2,'GRP00000000000000000','MNU01',1,NULL),
 (3,'GRP00000000000000000','MNU02',1,NULL),
 (4,'GRP00000000000000000','CHP002',1,NULL),
 (5,'GRP00000000000000000','CHP001',1,NULL),
 (6,'GRP_KOPERASI_ADMIN','TRS001',1,NULL),
 (7,'GRP_KOPERASI_ADMIN','TRS004',1,NULL),
 (9,'GRP_KOPERASI_ADMIN','TRS002',1,NULL),
 (10,'GRP_KOPERASI_ADMIN','TRS005',1,NULL),
 (11,'GRP_KOPERASI_ADMIN','TRS003',1,NULL),
 (12,'GRP_KOPERASI_ADMIN','DSB001',1,NULL),
 (13,'GRP_KOPERASI_ADMIN','DSB002',1,NULL),
 (14,'GRP_KOPERASI_ADMIN','DSB003',1,NULL),
 (15,'GRP_KOPERASI_ADMIN','TRS006',1,NULL);
/*!40000 ALTER TABLE `userAccessLevel` ENABLE KEYS */;


--
-- Definition of table `userGroup`
--

DROP TABLE IF EXISTS `userGroup`;
CREATE TABLE `userGroup` (
  `userGroupID` int(6) NOT NULL AUTO_INCREMENT,
  `userID` varchar(20) NOT NULL,
  `groupID` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userGroupID`),
  UNIQUE KEY `unq_userID_groupID` (`userID`,`groupID`),
  KEY `idx_userID_groupID` (`groupID`,`userID`),
  KEY `fk_userGroup_user_userID` (`userID`),
  KEY `fk_group_user_groupID` (`groupID`),
  KEY `fk_userGroup_userID_user_userID_idx` (`userID`),
  KEY `fk_userGroup_groupID_group_groupID_idx` (`groupID`),
  CONSTRAINT `fk_userGroup_groupID_group_groupID` FOREIGN KEY (`groupID`) REFERENCES `group` (`groupID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_userGroup_userID_user_userID` FOREIGN KEY (`userID`) REFERENCES `user` (`userID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `userGroup`
--

/*!40000 ALTER TABLE `userGroup` DISABLE KEYS */;
INSERT INTO `userGroup` (`userGroupID`,`userID`,`groupID`,`status`) VALUES 
 (3,'adminkoperasi','GRP00000000000000000',1),
 (4,'adminkoperasi','GRP_KOPERASI_ADMIN',0);
/*!40000 ALTER TABLE `userGroup` ENABLE KEYS */;


--
-- Definition of table `userMenu`
--

DROP TABLE IF EXISTS `userMenu`;
CREATE TABLE `userMenu` (
  `userMenuID` smallint(3) NOT NULL AUTO_INCREMENT,
  `menuID` smallint(3) NOT NULL,
  `groupID` varchar(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`userMenuID`),
  UNIQUE KEY `unq_menuID_groupID` (`menuID`,`groupID`),
  KEY `idx_menuID_groupID` (`menuID`,`groupID`),
  KEY `fk_userMenu_menu_menuID` (`menuID`),
  KEY `fk_userMenu_group1` (`groupID`),
  KEY `fk_userMenu_menuID_menu_id_idx` (`menuID`),
  KEY `fk_userMenu_groupID_group_groupID_idx` (`groupID`),
  CONSTRAINT `fk_userMenu_groupID_group_groupID` FOREIGN KEY (`groupID`) REFERENCES `group` (`groupID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_userMenu_menuID_menu_id` FOREIGN KEY (`menuID`) REFERENCES `menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `userMenu`
--

/*!40000 ALTER TABLE `userMenu` DISABLE KEYS */;
INSERT INTO `userMenu` (`userMenuID`,`menuID`,`groupID`,`status`) VALUES 
 (1,1,'GRP_KOPERASI_ADMIN',1),
 (6,2,'GRP_KOPERASI_ADMIN',1),
 (8,4,'GRP_KOPERASI_ADMIN',1),
 (9,5,'GRP_KOPERASI_ADMIN',1);
/*!40000 ALTER TABLE `userMenu` ENABLE KEYS */;


--
-- Definition of function `getNextID`
--

DROP FUNCTION IF EXISTS `getNextID`;

DELIMITER $$

/*!50003 SET @TEMP_SQL_MODE=@@SQL_MODE, SQL_MODE='' */ $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getNextID`(_tableName varchar(50),_initialID bool) RETURNS varchar(40) CHARSET utf8
    COMMENT '4 untuk pertahun'
BEGIN

    SET @strIDLength = 16;
    SET @result = '';
    SET @i = 1;
    SET @strDate = '';
    SET @debug = 0;
    SET @strDebug = '';

    SELECT nextID,initial,lastUpdate,resetID INTO @nextID,@initial,@lastUpdate,@resetID FROM counterDB WHERE tableName = _tableName;
    SET @i = @nextID;
    CASE @resetID

        WHEN  1 THEN
            SET @strDate = CONCAT(YEAR(CURDATE()),LPAD(MONTH(CURDATE()),2,'0'),LPAD(DATE_FORMAT(now(),'%d'),2,'0'));

        WHEN  2 THEN
            SET @strDate = CONCAT( YEAR( CURDATE() ), WEEKOFYEAR( CURDATE() ) );

        WHEN  3 THEN
            SET @strDate = CONCAT( YEAR( CURDATE() ),MONTH( CURDATE() ) );

        WHEN  4 THEN
            SET @strDate = YEAR(CURDATE());

        ELSE
            SET @strDate = '';
    END CASE;



    IF(@lastUpdate < CURDATE()) THEN

        CASE @resetID

            WHEN  0 THEN
                SET @i = @nextID;


            WHEN  1 THEN
                SET @i = 1;
                SET @strDate = CONCAT(YEAR(CURDATE()),LPAD(MONTH(CURDATE()),2,'0'),LPAD(DATE_FORMAT(now(),'%d'),2,'0'));
                IF ( @debug = 1 ) THEN
                    SET @strDebug = 'perhari';
                END IF;

            WHEN  2 THEN
                IF ( WEEKOFYEAR(@lastUpdate)!= WEEKOFYEAR(CURDATE())  ) THEN
                    SET @i = 1;
                END IF;
                IF ( @debug = 1 ) THEN
                    SET @strDebug = 'perminggu';
                END IF;

            WHEN  3 THEN
                IF (MONTH(@lastUpdate) != MONTH(CURDATE()) )  THEN
                    SET @i = 1;
                END IF;
                IF ( @debug = 1 ) THEN
                    SET @strDebug = 'perbulan';
                END IF;

            WHEN  4 THEN
                IF ( YEAR(@lastUpdate) != YEAR(CURDATE())  ) THEN
                    SET @i = 1;
                END IF;
                IF ( @debug = 1 ) THEN
                    SET @strDebug = 'pertahun';
                END IF;
            ELSE

                SET @result = '';

        END CASE;
    ELSE

        SET @i = @nextID;
        IF ( @debug = 1 ) THEN
            SET @strDebug = 'tanpa waktu';
        END IF;
    END IF;




    UPDATE counterDB
    SET nextID = @i + 1,lastUpdate = CURDATE()
    WHERE  counterDB.tableName = _tableName;

    IF (_initialID)  THEN
        SET @result = CONCAT(@initial,@strDate,LPAD(@i,16-LENGTH(CONCAT(@initial,@strDate)),'0'));

    ELSE
        SET @result = @i;
    END IF;

    IF ( @debug = 1 ) THEN
    	RETURN concat(@result,'|',@strDebug);
    ELSE
	RETURN @result;
    END IF;
END $$
/*!50003 SET SESSION SQL_MODE=@TEMP_SQL_MODE */  $$

DELIMITER ;

--
-- Definition of view `vUserAccessLevel`
--

DROP TABLE IF EXISTS `vUserAccessLevel`;
DROP VIEW IF EXISTS `vUserAccessLevel`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vUserAccessLevel` AS select `usg`.`userID` AS `userID`,`usr`.`realName` AS `realname`,`usg`.`groupID` AS `groupID`,`grp`.`name` AS `name`,`pgr`.`pageID` AS `pageID`,`pgr`.`filename` AS `filename`,`ual`.`active` AS `active` from ((((`userGroup` `usg` left join `group` `grp` on((`grp`.`groupID` = `usg`.`groupID`))) left join `userAccessLevel` `ual` on((`ual`.`groupID` = `grp`.`groupID`))) left join `pageRegistered` `pgr` on((`pgr`.`pageID` = `ual`.`pageID`))) left join `user` `usr` on((`usr`.`userID` = `usg`.`userID`))) where (`pgr`.`pageID` is not null);

--
-- Definition of view `vUserMenu`
--

DROP TABLE IF EXISTS `vUserMenu`;
DROP VIEW IF EXISTS `vUserMenu`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vUserMenu` AS select `usr`.`userID` AS `userID`,`usr`.`realName` AS `realname`,`grp`.`groupID` AS `groupID`,`grp`.`name` AS `name`,`mnu`.`status` AS `status`,`mnu`.`id` AS `id`,`mnu`.`parentID` AS `parentID`,`mnu`.`icon` AS `icon`,`mnu`.`title` AS `title`,`mnu`.`url` AS `url`,`mnu`.`menuOrder` AS `menuOrder`,`mnu`.`keterangan` AS `keterangan` from ((((`userMenu` `usm` left join `menu` `mnu` on((`mnu`.`id` = `usm`.`menuID`))) left join `group` `grp` on((`grp`.`groupID` = `usm`.`groupID`))) left join `userGroup` `usg` on((`usg`.`groupID` = `grp`.`groupID`))) left join `user` `usr` on((`usr`.`userID` = `usg`.`userID`)));



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
